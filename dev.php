<?php

require __DIR__ . "/vendor/autoload.php";

if (php_sapi_name() != "cli") {
    die("run this script only from cli, not from " . php_sapi_name());
}

const API_URL = "https://jsonplaceholder.typicode.com";
const GET = 'GET';
const NL = "\n";

const BASE_HREF = '/';
const EXT_HTML = '.html';

// echo API_URL;

// var_dump($argv);

if (count($argv) < 1) {
    echo "use " . $argv[0] . " id:int" . NL;
    die("@TODO: user input from cli");
} else {
    $userId = $argv[1];
    // @TODO: parse to int
}

// echo 'Say hello: ';

// $answer = Seld\CliPrompt\CliPrompt::hiddenPrompt();

// echo 'You answered: ' . $answer . PHP_EOL;

function get_albums(int $userId): \GuzzleHttp\Psr7\Stream
{
    $url = API_URL . "/albums?userId=$userId";

    return http_get($url);

}

function get_photos(int $albumId): \GuzzleHttp\Psr7\Stream
{
    $url = API_URL . "/photos?albumId=$albumId";

    return http_get($url);

}

function http_get(string $url): \GuzzleHttp\Psr7\Stream
{

    // ------------- very simply solution ---------------------
    // $data = file_get_contents($url);

    // ------------- php-http extension ---------------------

    // direct using php-http extension - not works - see Note.md

    // $extensions = get_loaded_extensions();

    // $ext_funcs = get_extension_funcs('http');

    // var_dump($extensions);
    // var_dump($ext_funcs);

    // $r = new HttpRequest($url, HttpRequest::METH_GET);
    // $r->addQueryData(array('userId' => $userId));

    // try {
    //     $r->send();
    //     if ($r->getResponseCode() == 200) {
    //         echo $r->getResponseBody();
    //     }
    // } catch (HttpException $ex) {
    //     echo $ex;
    // }

    // ------------- via guzzle library ---------------------

    $client = new \GuzzleHttp\Client(['http_errors' => true]);
    $request = $client->request(GET, $url);

    $http_code = $request->getStatusCode();

    // dont need if $client = new \GuzzleHttp\Client(['http_errors' => true]);
    // if ($http_code >= 400) {
    //     throw new \Exception("Http error code $http_code for $url");
    // }

    $content_type = trim(
        explode(
            ';',
            $request->getHeaderLine('content-type')
        )[0]
    ); # 'application/json; charset=utf8'

    if ($content_type != 'application/json') {
        throw new \Exception("Content type $content_type failed. Expect application/json.");
    }

    $data = $request->getBody();

    // foreach ($data as $album) {
    //     yield $album;
    // }
    return $data;
}

$albums = get_albums($userId)->getContents();

$albums = json_decode($albums, true/*, JSON_THROW_ON_ERROR Available since PHP 7.3.0. */);

// echo $albums;

const ID = 'id';
const TITLE = 'title';

use \Cocur\Slugify\Slugify;

$slugify = new Slugify();

function slug_url(string $title): string
{
    global $slugify;
    return BASE_HREF . $slugify->slugify($title) . EXT_HTML;
}

function write_index(int $userId, array $albums)
{

    $source = __DIR__ . '/templates/index.phtml';
    $output = __DIR__ . '/output/index.html';

    if (!file_exists($source)) {
        throw new \Exception("Template file $source not found.");
    }

    ob_start();
    include $source;
    $code = ob_get_clean();

    file_put_contents($output, $code);

}

function write_page(int $userId, array $album, array $photos)
{

    $source = __DIR__ . '/templates/album.phtml';
    $output = __DIR__ . '/output' . slug_url($album[TITLE]);

    if (!file_exists($source)) {
        throw new \Exception("Template file $source not found.");
    }

    ob_start();
    include $source;
    $code = ob_get_clean();

    file_put_contents($output, $code);

}

write_index($userId, $albums);

foreach ($albums as $album) {
    // var_dump($album);
    // echo $album[TITLE] . NL;

    $photos = get_photos($album[ID])->getContents();
    $photos = json_decode($photos, true/*, JSON_THROW_ON_ERROR Available since PHP 7.3.0. */);
    // var_dump($photos);
    write_page($userId, $album, $photos);
}
