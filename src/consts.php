<?php

const NL = "\n";

// --- api

const API_URL = "https://jsonplaceholder.typicode.com";

// --- keys

const GET = 'GET';
const ID = 'id';
const TITLE = 'title';

// --- file names and extensions

const EXT_HTML = '.html';

// --- paths

const BASE_HREF = '/';
const OUTPUT_DIR = __DIR__ . '/../output';
