
# generate-album-computer-help

example app for new jobs

```bash
$ php generateAlbum.php
$ cd ./output
$ php -S localhost:8889
```
and open in browser

## Obecné požadavky

1. Aplikace by měla být snadno spustitelná. Pokud je to možné, připravte aplikaci v Docker containeru. Popište instalační proceduru.
2. Nepoužívejte žádné frameworky. Samostatné knihovny použít lze.
3. Přidejte komentář o Vámi použitém řešení. Zvažte výhody a nevýhody konkrétního řešení.
4. Kód by měl být alespoň v PHP 5.6, ale volba je na Vás.
5. Nezapomeňte ošetřit možné chyby a výjimky.
6. Všechny komentáře a kód musí být v angličtině.
7. máte 7 dní na odevzdání úkolu. Pokud jej nestihnete dořešit, pošlete co máte. Cílem není funkční aplikace, ale ukázka Vašeho “rukopisu”.
8. Myslete na to, že chcete ukázat jak ovládáte OOP a jeho možnosti. Nebojte se použít “overkill” řešení. To je cílem ;)

## Funkční zadání

Vytvořte CLI aplikaci, která zpracovává API (https://jsonplaceholder.typicode.com/). Aplikace by měla v parametru obdržet userId a následně z API načíst alba uživatele a jednotlivé fotky. Z toho následně sestavit html dokument, ve kterém budou jednotlivá alba vypsána včetně fotek.

## ukázka volání

takto by mohlo například vypadat volání generování alba uživatele 1

```bash
$ # php generateAlbum.php 1
```