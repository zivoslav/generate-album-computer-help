<?php

require __DIR__ . "/vendor/autoload.php";

if (php_sapi_name() != "cli") {
    die("run this script only from cli, not from " . php_sapi_name());
}

require_once __DIR__ . '/src/consts.php';
require_once __DIR__ . '/src/slugify.php';
require_once __DIR__ . '/src/api.php';
require_once __DIR__ . '/src/write.php';

if (count($argv) < 1) {
    echo "use " . $argv[0] . " id:int" . NL;
    die("@TODO: user input from cli");
} else {
    $userId = $argv[1];
    // @TODO: parse to int
}

$albums = decode_stream(get_albums($userId));

// echo $albums;

write_index($userId, $albums);

foreach ($albums as $album) {
    // var_dump($album);
    // echo $album[TITLE] . NL;

    $photos = decode_stream(get_photos($album[ID]));
    // var_dump($photos);
    write_page($userId, $album, $photos);
}

echo "all pages generetaed. Go to ./output and run server examply 'php -S localhost:8889'";
