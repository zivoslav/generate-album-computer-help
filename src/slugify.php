<?php

use \Cocur\Slugify\Slugify;

$slugify = new Slugify();

function slug_url(string $title): string
{
    global $slugify;
    return BASE_HREF . $slugify->slugify($title) . EXT_HTML;
}
