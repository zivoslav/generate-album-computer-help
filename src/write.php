<?php

function write_index(int $userId, array $albums)
{

    $source = __DIR__ . '/templates/index.phtml';
    $output = OUTPUT_DIR . '/index.html';

    if (!file_exists($source)) {
        throw new \Exception("Template file $source not found.");
    }

    ob_start();
    include $source;
    $code = ob_get_clean();

    file_put_contents($output, $code);

}

function write_page(int $userId, array $album, array $photos)
{

    $source = __DIR__ . '/templates/album.phtml';
    $output = OUTPUT_DIR . slug_url($album[TITLE]);

    if (!file_exists($source)) {
        throw new \Exception("Template file $source not found.");
    }

    ob_start();
    include $source;
    $code = ob_get_clean();

    file_put_contents($output, $code);

}
